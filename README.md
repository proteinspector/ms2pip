# THERE IS A NEW VERSION OF MS2PIP IN THE MS2TOOLBOX PACKAGE, PLEASE USE THAT ONE #
## I keep this version online because Alexander used this version for his thesis. ##


## MS2PIP

### *Short description*

MS2PIP is a tool for predicting MS/MS signal peak intensities from peptide sequences. It can train prediction models from peptide-spectrum-match (PSM) data and/or predict fragment ion intensities for peptides. MS2PIP employs the Random Forest regression (RF) algorithm for building the predictive models. The package includes RF models for predicting CID MS2 peak intensities for charge +2 peptides with length within [6,28] and +3 charged peptides with length within [8,28].

### *Installation*

MS2PIP was written in python (2.6.5) on a Ubuntu 10.04 64Bit linux machine.

MS2PIP requires the [scikit-learn](http://scikit-learn.org/stable/install.html) to be installed.

Unzip the archive or fork the code to a local dir, e.g. ~/MS2PIP. 
Download the CID RF models and put them in a directory called ~/MS2PIP/CIDMODELS

### *Usage*

#### Training MS2 peak intensity prediction models

MS2PIP model building is invoked by the `train.py` python script:

Usage: `train.py [options]`

Options:

  `-h, --help         show this help message and exit`

  `-f FILE  PSM dataset`

  `-l INT   PSM peptide length`

  `-c INT   PSM charge state`


MS2PIP reads the PSMs from a flat train-input file pointed to by the -f command line option. Each train-input file contains PSMs with the same peptide length and charge state, indicated by the -l and -c options (these are required). So, running 

`$ train.py -f train-input.example -l 12 -c 3`

builds RF models for predicting the b- and y-ion TIC normalized peak intensities for all peptides with length 12 and charge +3. The file train-input.file then contains only PSMs with peptide length 12 and charge state +3. Each row in a train-input file is one PSM. The first column is the peptide sequence and the second column is a unique identifier for the PSM. The next columns contain the TIC normalized MS2 peak intensities for the b-ions and the y-ions. These must be in this order, so for peptides of lenght N the third column contains the observed TIC normalized peak intensity for b1 and the last column contains the TIC normalized intensitiy observed for y(N-1). For the sequence column the modifications of the amino acids are written as follows:
a modified amino acid A is written as A<X> with X the mass change caused by the modification. An n-terminally modified peptide SAMPLE is written as <X>SAMPLE with X the mass change caused by the n-terminal modification.

The compressed RF models computed by `train.py` are written as `<train-input.example>.C.L.X.pkl` files, with C the charge state, L the peptide length and X the fragment ion the RF model corresponds to.

Additionally, `train.py` also outputs files `<train-input-file>.C.L.features` and `<train-input-file>.C.L.featuresspecific` that contain the features used by each RF model.

All these files are required by `predict.py` (see further).

#### Predicting MS2 peak intensities

To predict MS2 peak intensities from peptide sequences the `predict.py` script requires the pkl and feature files written by `train.py`. The `predict.py` script is invoked as:

Usage: `predict.py [options]`

Options:

  `-h, --help show this help message and exit`

  `-f FILE peptide dataset`

  `-m FILE RF models directory`

  `-c INT number of CPU's/cores`

The directory that contains the pkl and feature files is provided by the `–m` option. The predict-input file contains two columns. The first column contains the peptide sequence from which the MS2 peaks need to be predicted. The second column contains the desired charge state.  

The `predict.py` script writes the prediction results as an MGF file. For each predicted spectrum the script writes the id (in the title field), peptide mass (in the pepmass field), the charge state (in the charge field) and then the masses and predicted intensities of the fragment ions.

The `predict.py` script can use more than one cpu/core as specified by the -c option.

### *Example*

The MS2PIP package contains example input-files in the ~/MS2PIP/EXAMPLES/ directory.

### *Limitations*

* The CID RF models are trained on typicall COFRADIC data.
* The CID RF models are trained on examples with specific COFRADIC PTMs.

### *Good to know*

For training, make sure to normalize the MS2 spectra using Total Ion Count (TIC) normalization.