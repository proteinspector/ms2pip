import sys
import numpy as np
import MS2PIPfeatures
from math import log
from optparse import OptionParser
from sklearn.ensemble import RandomForestRegressor
from sklearn.externals.six import StringIO  
from sklearn.externals import joblib
from scipy.stats import *

parser = OptionParser()
parser.add_option("-f", dest="filename",help="PSM dataset", metavar="FILE")
parser.add_option("-l", dest="peplen",help="PSM peptide length", metavar="INT")
parser.add_option("-c", dest="charge",help="PSM charge state", metavar="INT")

(options, args) = parser.parse_args()
if not options.filename:
    parser.error('Filename not given! (use -h for options)')
if not options.l:
    parser.error('No peptide length! (use -h for options)')
if not options.c:
    parser.error('No charge state! (use -h for options)')


print "Reading data"
peptides = {}
peptideids = {}
targets = {}
f = open(options.filename)
for line in f:
        tmp = line.rstrip().split()
	peptides[tmp[0]] = 1
	try:
		t = peptideids[tmp[0]]
	except:
		peptideids[tmp[0]] = []
	peptideids[tmp[0]].append(tmp[1])
	targets[tmp[1]] = []
	for i in range(2,len(tmp),1):
		targets[tmp[1]].append(float(tmp[i]))	

print "Computing feature vectors"
features = {}
featuresSpecific = {}
vectors = {}
vectorsSpecific = {}
for seq in peptides:
	vectors[seq] = MS2PIPfeatures.computeVector(seq)
	vectorsSpecific[seq] = MS2PIPfeatures.computeVectorSpecific(seq)
	for f in vectors[seq].keys():
		features[f] = 1
	for i in vectorsSpecific[seq]:
		for f in i.keys():
			featuresSpecific[f] = 1
fl = open(options.filename + "." + options.charge + "." + options.peplen + '.features', "w")
for f in sorted(features.keys()):
	fl.write(f + "\n")
fl.close()
fl = open(options.filename  + "." + options.charge + "." + options.peplen + '.featuresspecific', "w")
for f in sorted(featuresSpecific.keys()):
	fl.write(f + "\n")
fl.close()

print "Merging targets"
mergedtargets = {}
for seq in peptides:
	mergedtargets[seq] = []
	for i in range(0,(int(options.peplen)-1)*2,1):
		buf = []
		for id in peptideids[seq]:
			buf.append(targets[id][i])
		sorts = sorted(buf)
    		length = len(sorts)
    		if not length % 2:
        		mergedtargets[seq].append(log(((sorts[length / 2] + sorts[length / 2 - 1]) / 2.0) + 0.001) / log(2))
		else:
    			mergedtargets[seq].append(log((sorts[length / 2]) + 0.001) / log(2))

print "Creating numPy datasets"
X = []
for seq in peptides:
	buf = []
	for f in sorted(features.keys()):
		try:
			buf.append(vectors[seq][f])
		except:
			buf.append(0);
	X.append(buf)
numpyX = np.array(X)
XS = []
for i in range((int(options.peplen)-1)*2):
	X = []
	for seq in peptides:
		buf = []
		for f in sorted(featuresSpecific.keys()):
			try:
				buf.append(vectorsSpecific[seq][i][f])
			except:
				buf.append(0);
		X.append(buf)
	XS.append(np.array(X))
Y = []
for seq in peptides:
	buf = []
	for i in range(0,(int(options.peplen)-1)*2,1):
		buf.append(mergedtargets[seq][i])	
	Y.append(buf)
numpyY = np.array(Y)

numfeats = len(features)

for i in range(0,(int(options.peplen)-1)*2,1):
	print i

	trainX = np.hstack((numpyX,XS[i]))

        bestleng = 0
	bestmaxf = 0
	maxv = 0
	for leng in [50,100]:
                for maxff in [0.01,0.05,0.1,0.3,0.6,0.9]:
                        maxf = int(round(numfeats * maxff))
			clf_1 = RandomForestRegressor(n_estimators=leng,max_features=maxf,min_samples_split=3,n_jobs=-1,verbose=0,oob_score=True)
			clf_1.fit(trainX, numpyY[:,i])
			y = clf_1.oob_prediction_
			l = numpyY[:,i]
			v = np.var(l)
			mse = ((y-l)**2).mean()
			R2 = 1 - (mse/v)
			RP = pearsonr(y,l)
			RS = spearmanr(y,l)
			print "%i %i %f %f %f" % (leng,maxf,R2,RP,RS)
			if (R2 > maxv):
				maxv = R2
				bestleng = leng
				bestmaxf = maxf

	print "Training %i %i %i %f" % (i,bestleng,bestmaxf,maxv)

	if (bestleng == 0):
		continue

	clf = RandomForestRegressor(n_estimators=bestleng,max_features=bestmaxf,n_jobs=24,min_samples_split=3,verbose=0)
	clf.fit(trainX, numpyY[:,i])
	joblib.dump(clf, options.filename  + "." + options.charge + "." + options.peplen + '.' + str(i) + '.pkl', compress=9)
	
