import sys
import numpy as np
import re
from math import log
from optparse import OptionParser
from sklearn.ensemble import RandomForestRegressor
from sklearn.externals.six import StringIO
from sklearn.externals import joblib
import glob
import MS2PIPfeatures
import multiprocessing

parser = OptionParser()
parser.add_option("-f", dest="filename",
                  help="peptide dataset", metavar="FILE")
parser.add_option("-m",  dest="models",
                  help="RF models directory", metavar="DIR")
parser.add_option("-c",  dest="numcpu",
                  help="number of CPU's/cores", metavar="INT",default=1)
parser.add_option("-o",  dest="outputformat",
                  help="format of output: ['mgf','csv']", metavar="STRING",default="csv")

(options, args) = parser.parse_args()
if not options.filename:
    parser.error('Filename not given! (use -h for options)')
if not options.models:
    parser.error('No RF models directory given! (use -h for options)')


#read RF models
RFmodels = {}
files = glob.glob(str(options.models) + "/*.pkl")
for file in files:
	tmp = file.split("/")
	tmp2 = tmp[-1].split(".")
	chargetmp = tmp2[-4]
	lengthtmp = tmp2[-3] 
	iontmp = int(tmp2[-2])
	
	try:	
		t = RFmodels[chargetmp]
	except KeyError:
		RFmodels[chargetmp] = {}

	try:	
		t = RFmodels[chargetmp][lengthtmp]
	except KeyError:
		RFmodels[chargetmp][lengthtmp] = {}

	try:	
		RFmodels[chargetmp][lengthtmp][iontmp] = file
	except IndexError:
		RFmodels[chargetmp][lengthtmp] = []
		RFmodels[chargetmp][lengthtmp][iontmp] = file

#read RF features
RFfeatures = {}
files = glob.glob(str(options.models) + "/*.features")
for file in files:
	tmp = file.split("/")
	tmp2 = tmp[-1].split(".")
	chargetmp = tmp2[-3]
	lengthtmp = tmp2[-2] 

	try:	
		t = RFfeatures[chargetmp]
	except KeyError:
		RFfeatures[chargetmp] = {}

	try:	
		t = RFfeatures[chargetmp][lengthtmp]
	except KeyError:
		RFfeatures[chargetmp][lengthtmp] = []
		tel = 0
		f = open(file)
		for line in f:
			tmp = line.rstrip().split()
			RFfeatures[chargetmp][lengthtmp].append(tmp[0])
			tel+=1
		f.close()
		#print lengthtmp + " " + chargetmp + " " + str(tel)

#read RF features specific
RFfeaturesspecific = {}
files = glob.glob(str(options.models) + "/*.featuresspecific")
for file in files:
	tmp = file.split("/")
	tmp2 = tmp[-1].split(".")
	chargetmp = tmp2[-3]
	lengthtmp = tmp2[-2] 
	
	try:	
		t = RFfeaturesspecific[chargetmp]
	except KeyError:
		RFfeaturesspecific[chargetmp] = {}

	try:	
		t = RFfeaturesspecific[chargetmp][lengthtmp]
	except KeyError:
		RFfeaturesspecific[chargetmp][lengthtmp] = []
		f = open(file)
		tel = 0
		for line in f:
			tmp = line.rstrip().split()
			RFfeaturesspecific[chargetmp][lengthtmp].append(tmp[0])
			tel+=1
		f.close()
		#print lengthtmp + " " + chargetmp + " " + str(tel)

#read the data
seqs = {}
seqlens = {}
f = open(options.filename)
for line in f:
	tmp = line.rstrip().split()
	if (len(tmp) < 2):
		continue

	if ((tmp[1] != "2") & (tmp[1] != "3")):
		print "Warning: ms2pip only predicts MS2 spectra for charge state +2 and +3! Peptide skipped..."

	el = re.sub('<[^<]+?>', '', tmp[0])	

	if ((len(el) < 6) | (len(el) > 28)):
		print "Warning: ms2pip only predicts MS2 spectra for peptides with length within [8,28]! Peptide skipped..."

	try:
		t = seqlens[tmp[1]]
	except KeyError:
		seqlens[tmp[1]] = {}

	try:
		t = seqs[tmp[1]]
	except KeyError:
		seqs[tmp[1]] = []

	seqlens[tmp[1]][str(len(el))] = 1
	seqs[tmp[1]].append(tmp[0])
f.close()

pepmass = {}

def predictMS2PIP(c,l):
	global seqs
	global RFmodels
	global RFfeatures
	global RFfeaturesspecific
	global pepmass

	sys.stderr.write("\nPredicting %s %s" % (c,l))

	charhe = int(c)
	peplen = int(l)

	testX = {}
	testXS = {}
	for seq in seqs[c]:
		el = re.sub('<[^<]+?>', '', seq)	
		pl = len(el)
		if (pl != peplen):
			continue

		X = MS2PIPfeatures.computeVector(seq)
		testX[seq] = X
		pepmass[seq] = X["pep_mz"]
		XS = MS2PIPfeatures.computeVectorSpecific(seq)
		testXS[seq] = XS

	#create scikit dataset
	X = []
	seqsmap = []
	for seq in testX.keys():
		seqsmap.append(seq)
		XX = []
		for f in sorted(RFfeatures[c][l]):
			if f in testX[seq]:
				XX.append(testX[seq][f])
			else:
				XX.append(0)
		X.append(XX)
	numpyX = np.array(X)					

	#predict
	numions = (peplen-1)*2
	result = []
	for i in range(numions):
		#sys.stderr.write(".")
		try:
			scikitmodel = joblib.load(RFmodels[c][l][i])
		except:
			sys.stderr.write(str(i))
			Y = []
			for seq in testXS.keys():
				Y.append(log(0.001)/log(2))
			result.append(Y)
			continue

		X = []
		for seq in testXS.keys():
			XX = []
			for f in sorted(RFfeaturesspecific[c][l]):
				if f in testXS[seq][i]:
					XX.append(testXS[seq][i][f])
				else:
					XX.append(0)
			X.append(XX)
		numpyXS = np.array(X)
		numpyTest = np.hstack((numpyX,numpyXS))
		scikitmodel.set_params(n_jobs=1)
		result.append(scikitmodel.predict(numpyTest))
	buf = ""		
	if (options.outputformat == "mgf"):
		for r in range(len(seqsmap)):
			buf += "BEGIN IONS\n"
			buf += "PEPMASS=" + str(pepmass[seqsmap[r]]) + "\n"
			buf += "CHARGE=" + c + "\n"
			buf += "TITLE=" + seqsmap[r] + "\n"
			
			sum = 0
			el = re.sub('<[^<]+?>', '', seq)	
			for i in range(peplen-1):
				sum += MS2PIPfeatures.mass[el[i]]
				j = i + 1
				tmpsum = sum + 1
				buf += str(tmpsum) + " " + str(result[i][r]) + " b(" + str(j) + ")\n"								
			sum = 0
			for i in range(peplen-1):
				sum += MS2PIPfeatures.mass[el[peplen-1-i]]
				j = i + 1
				tmpsum = sum + 19
				buf+= str(tmpsum) + " " + str(result[peplen+i-1][r]) + " y(" + str(j) + ")\n"								
			buf += "END IONS\n"
		return buf
	if (options.outputformat == "csv"):
		for r in range(len(seqsmap)):
			buf += seqsmap[r]
			buf += " " + c
			
			for i in range(peplen-1):
				buf += " " + str(result[i][r])								
			for i in range(peplen-1):
				buf += " " + str(result[peplen+i-1][r])								
			buf += "\n"
		return buf

mgf = {}
mgf['2'] = {}
mgf['3'] = {}

myPool = multiprocessing.Pool(int(options.numcpu))
	
for charge in seqlens.keys():
	for length in seqlens[charge].keys():
		mgf[charge][length] = myPool.apply_async(predictMS2PIP,(charge,length))

myPool.close()
myPool.join()

for charge in seqlens.keys():
	for length in seqlens[charge].keys():
		print str(mgf[charge][length].get())
