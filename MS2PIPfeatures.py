import re

mass = {}
mass["A"] = 71.037114
mass["B"] =  114.53494
mass["C"] =  103.009185
mass["D"] =  115.026943
mass["E"] =  129.042593
mass["F"] =  147.068414
mass["G"] =  57.021464
mass["H"] =  137.05891
mass["I"] =  113.084064
mass["J"] =  0.0
mass["K"] =  128.094963
mass["L"] =  113.084064
mass["M"] =  131.040485
mass["N"] =  114.042927
mass["O"] =  0.0
mass["P"] =  97.052764
mass["Q"] =  128.058578
mass["R"] =  156.101111
mass["S"] =  87.032028
mass["T"] =  101.047679
mass["U"] =  147.0354
mass["V"] =  99.068414
mass["W"] =  186.079313
mass["X"] =  111.0
mass["Y"] =  163.063329
mass["Z"] =  128.55059

bas = {}
bas["A"] = 206.4
bas["B"] = 210.7
bas["C"] = 206.2
bas["D"] = 208.6
bas["E"] = 215.6
bas["F"] = 212.1
bas["G"] = 202.7
bas["H"] = 223.7
bas["I"] = 210.8
bas["K"] = 221.8
bas["L"] = 209.6
bas["M"] = 213.3
bas["N"] = 212.8
bas["P"] = 214.4
bas["Q"] = 214.2
bas["R"] = 237.0
bas["S"] = 207.6
bas["T"] = 211.7
bas["V"] = 208.7
bas["W"] = 216.1
bas["X"] = 210.2
bas["Y"] = 213.1
bas["Z"] = 214.9

hydro = {}
hydro["A"] = 0.16
hydro["B"] = -3.14
hydro["C"] = 2.50
hydro["D"] = -2.49
hydro["E"] = -1.50
hydro["F"] = 5.00
hydro["G"] = -3.31
hydro["H"] = -4.63
hydro["I"] = 4.41
hydro["K"] = -5.00
hydro["L"] = 4.76
hydro["M"] = 3.23
hydro["N"] = -3.79
hydro["P"] = -4.92
hydro["Q"] = -2.76
hydro["R"] = -2.77
hydro["S"] = -2.85
hydro["T"] = -1.08
hydro["V"] = 3.02
hydro["W"] = 4.88
hydro["X"] = 4.59
hydro["Y"] = 2.00
hydro["Z"] = -2.13

heli = {}
heli["A"] = 1.24
heli["B"] = 0.92
heli["C"] = 0.79
heli["D"] = 0.89
heli["E"] = 0.85
heli["F"] = 1.26
heli["G"] = 1.15
heli["H"] = 0.97
heli["I"] = 1.29
heli["K"] = 0.88
heli["L"] = 1.28
heli["M"] = 1.22
heli["N"] = 0.94
heli["P"] = 0.57
heli["Q"] = 0.96
heli["R"] = 0.95
heli["S"] = 1.00
heli["T"] = 1.09
heli["V"] = 1.27
heli["W"] = 1.07
heli["X"] = 1.29
heli["Y"] = 1.11
heli["Z"] = 0.91

pI = {}
pI["A"] = 6.00
pI["C"] = 5.07
pI["D"] = 2.77
pI["E"] = 3.22
pI["F"] = 5.48
pI["G"] = 5.97
pI["H"] = 7.59
pI["I"] = 6.02
pI["K"] = 9.74
pI["L"] = 5.98
pI["M"] = 5.74
pI["N"] = 5.41
pI["P"] = 6.30
pI["Q"] = 5.65
pI["R"] = 10.76
pI["S"] = 5.68
pI["T"] = 5.60
pI["V"] = 5.96
pI["W"] = 5.89
pI["Y"] = 5.66

def computeVector(seq):
	el = re.sub('<[^<]+?>', '', seq)
        peplen = len(el)

	result = {}
	label = "0"
	pos = -1
	if (seq[0] == "<"):
		label = ""
		pos = 1
		while (seq[pos] != '>'):
			label += str(seq[pos])
			pos += 1
	pepmz = float(label)
	f = "label"
	v = 0
	if (label != "0"):
		v = 1
	result[f] = v
	fpos = 1
	pos2 = pos
	avg_bas = 0
	avg_hydro = 0
	avg_heli = 0
	avg_pI = 0
	tel = 0
	pos3 = 0
	while (pos2 < len(seq)-1):
		pos3 += 1
		pos2 += 1				
		f = str(fpos) + "_" + seq[pos2]
		v = 1
		result[f] = v
		f = "I_" + seq[pos2]
		try:
			result[f]
		except KeyError:
			result[f] = 0
		else:
			result[f] += 1
		f = str(fpos) + "_bas"
		v = bas[seq[pos2]]
		result[f] = v
		f = str(fpos) + "_hydro"
		v = hydro[seq[pos2]]
		result[f] = v
		f = str(fpos) + "_heli"
		v = heli[seq[pos2]]
		result[f] = v
		f = str(fpos) + "_pI"
		v = pI[seq[pos2]]
		result[f] = v

		pepmz += mass[seq[pos2]]
		avg_bas += bas[seq[pos2]]
		avg_hydro += hydro[seq[pos2]]
		avg_heli += heli[seq[pos2]]
		avg_pI += pI[seq[pos2]]
		tel += 1
		if (pos2 == (len(seq))):
			continue
		if (pos2 < (len(seq)-1)): 
			if (seq[pos2+1] == "<"):
				amino = seq[pos2]
				ptm = ""
				pos2 += 2
				while ((seq[pos2] != '>') & (pos2 < len(seq))):
					ptm += str(seq[pos2])
					pos2 += 1
				if (ptm.find(',')!=-1):
					tmp = ptm.split(',')
					ptm2 = 0
					for i in range(len(tmp)):
						ptm2 += float(tmp[i])
					pepmz += ptm2
				else:	
					pepmz += float(ptm)
				f = str(fpos) + "_" + amino + "_ptm"
				v = 1
				result[f] = v
		fpos += 1
	avg_bas /= tel
	f = "avg_bas"
	v = avg_bas
	result[f] = v
	avg_hydro /= tel
	f = "avg_hydro"
	v = avg_hydro
	result[f] = v
	avg_heli /= tel
	f = "avg_heli"
	v = avg_heli
	result[f] = v
	avg_pI /= tel
	f = "avg_pI"
	v = avg_pI
	result[f] = v
	pepmz += 18
	f = "pep_mz"
	v = pepmz
	result[f] = v
	return result

def computeVectorSpecific(seq):
	el = re.sub('<[^<]+?>', '', seq)
        peplen = len(el)

	result = []
	for i in range(len(el)):
		result.append({})
	for i in range(len(el)):
		result.append({})

	sumbasb = 0
	sumhelib = 0
	sumhydrob = 0
	sumpib = 0
	for i in range(len(el)):
		sumbasb += bas[el[i]]
		sumhelib += heli[el[i]]
		sumhydrob += hydro[el[i]]
		sumpib += pI[el[i]]
		f = "avg_bas_ion"
		result[i][f] = sumbasb / (i+1)			
		f = "avg_heli_ion"
		result[i][f] = sumhelib / (i+1)			
		f = "avg_hydro_ion"
		result[i][f] = sumhydrob / (i+1)			
		f = "avg_pI_ion"
		result[i][f] = sumpib / (i+1)			
	sumbasb = 0
	sumhelib = 0
	sumhydrob = 0
	sumpib = 0
	for i in range(len(el)):
		l = len(el)
		sumbasb += bas[el[l-i-1]]
		sumhelib += heli[el[l-i-1]]
		sumhydrob += hydro[el[l-i-1]]
		sumpib += pI[el[l-i-1]]
		f = "avg_bas_ion"
		result[l+i][f] = sumbasb / (i+1)			
		f = "avg_heli_ion"
		result[l+i][f] = sumhelib / (i+1)			
		f = "avg_hydro_ion"
		result[l+i][f] = sumhydrob / (i+1)			
		f = "avg_pI_ion"
		result[l+i][f] = sumpib / (i+1)			

	pos = -1
	label = "0"
	if (seq[0] == "<"):
		label = ""
		pos = 1
		while (seq[pos] != '>'):
			label += str(seq[pos])
			pos += 1

	pos2 = pos
	pos3 = 0
	massbuf = []
	tel = 0
	while (pos2 < len(seq)-1):
		pos3 += 1
		pos2 += 1				

		if (tel == 0):
			massbuf.append(mass[seq[pos2]]+float(label))
		else:
			massbuf.append(mass[seq[pos2]])

		if (pos2 == (len(seq))):
			continue
		if (pos2 < (len(seq)-1)): 
			if (seq[pos2+1] == "<"):
				amino = seq[pos2]
				ptm = ""
				pos2 += 2
				while ((seq[pos2] != '>') & (pos2 < len(seq))):
					ptm += str(seq[pos2])
					pos2 += 1
				if (ptm.find(',')!=-1):
					tmp = ptm.split(',')
					ptm2 = 0
					for i in range(len(tmp)):
						ptm2 += float(tmp[i])
					massbuf[tel] += ptm2
				else:	
					massbuf[tel] += float(ptm)
		tel += 1

	sum = 0
	for i in range(len(el)):
		l = len(el)
		sum += massbuf[i]
		f = "ion_mz"
		result[i][f] = sum + 1	
	sum = 0
	for i in range(len(el)):
		l = len(el)
		sum += massbuf[l-i-1]
		f = "ion_mz"
		result[l+i-1][f] = sum + 19	
	return result

